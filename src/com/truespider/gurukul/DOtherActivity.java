/**
 * 
 */
package com.truespider.gurukul;

import com.truespider.gurukul.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DOtherActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_other);
		
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Other Activities");
		getActionBar().setCustomView(mCustomView);
		
		((ImageView) findViewById(R.id.img_other_sir)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DOtherActivity.this, DSirActivity.class));
			}
		});
		
		((ImageView) findViewById(R.id.img_other_palak)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DOtherActivity.this, DPalakActivity.class));
			}
		});

		((ImageView) findViewById(R.id.img_other_mahaexam)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DOtherActivity.this, DMahaExamActivity.class));
			}
		});

		((ImageView) findViewById(R.id.img_other_mahaparents)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DOtherActivity.this, DMahaParentsActivity.class));
			}
		});

	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
}
