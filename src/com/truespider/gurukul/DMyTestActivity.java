/**
 * 
 */
package com.truespider.gurukul;

import java.util.ArrayList;
import java.util.List;

import com.truespider.gurukul.R;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * @author HP
 *
 */
public class DMyTestActivity extends DBaseActivity {

	private WebView _content = null;
	
	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_mytest);
		
		_content = (WebView) findViewById(R.id.webView_my_test);
		_content.setWebChromeClient(new WebChromeClient(){
            @SuppressWarnings("deprecation")
			@Override
            public void onExceededDatabaseQuota(String url, String
                    databaseIdentifier, long currentQuota, long estimatedSize, long
                                                        totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(estimatedSize * 2);
            }
            
            
        });
        //content.addJavascriptInterface(new WebAppInterface(this), "Jasandra");
        WebSettings settings = _content.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(false);
        // Jellybean rightfully tried to lock this down. Too bad they didn't give us a whitelist
        // while we do this
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
            Level16Apis.enableUniversalAccess(settings);
        // Enable database
        // We keep this disabled because we use or shim to get around DOM_EXCEPTION_ERROR_16
        //String databasePath = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        String databasePath = "/data/data/com.truespider.dipper/databases/exam";
        settings.setDatabaseEnabled(true);
        //settings.setDatabasePath(databasePath);
        settings.setDatabasePath("/data/data/"+this.getPackageName()+"/databases/");
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        _content.setWebViewClient(new MyWebViewClient()); 
        _content.loadUrl("file:///android_asset/exam.html");
		
	}
	
	
	@TargetApi(16)
    private static class Level16Apis {
        static void enableUniversalAccess(WebSettings settings) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
    }
	
	 
	 private List<String> urls = new ArrayList<String>();
	 private class MyWebViewClient extends WebViewClient {
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        if (Uri.parse(url).getHost().equals("file:///android_asset/exam.html")) {
		            // This is my web site, so do not override; let my WebView load the page
		            return false;
		        }
		        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
		        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		        startActivity(intent);
		        return true;
		    }
		    
		    @Override
		    public void onPageFinished(WebView view, String url) {
		    	 urls.add(0, url);
		    }
		    
		    
		    
		    @Override
		    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
		    	 resend.sendToTarget();
		    }
		}

}
