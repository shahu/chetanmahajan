/**
 * 
 */
package com.truespider.gurukul.service;

import java.io.IOException;

import org.json.JSONObject;

import com.truespider.gurukul.model.DServiceNotification;
import com.truespider.gurukul.service.DRestClient.RequestMethod;

import android.os.AsyncTask;
import android.util.Log;

/**
 * @author HP
 *
 */
public class DServiceTask extends AsyncTask<String, String, String> {

	private final String TAG = DServiceTask.class.getName();
	private DServiceNotification _notification = null;
	private int _statusCode;
	private RequestMethod _requestMethod;
	private JSONObject _jsonObject = null;
	
	public DServiceTask(DServiceNotification notification, RequestMethod requestMethod, JSONObject jsonObject) {
		this._notification = notification;
		this._requestMethod = requestMethod;
		this._jsonObject = jsonObject;
	}

	
	@Override
	protected String doInBackground(String... params) {

		try {
			
			Log.i(TAG, "Request is : " + params[0]);			 
			DRestClient client = new DRestClient(params[0], _jsonObject);
			client.Execute(_requestMethod);
			 
			_statusCode = client.getResponseCode();
			
			String response = client.getResponse();
			Log.i(TAG, "Response is : " + response);
			return response;
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	protected void onPostExecute(String result) {
		if(result != null)
			_notification.serviceNotification(_statusCode, result);
		else
			_notification.serviceNotification(_statusCode, null);
	}
}