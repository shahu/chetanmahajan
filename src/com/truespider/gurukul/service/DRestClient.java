/**
 * 
 */
package com.truespider.gurukul.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

/**
 * @author HP
 *
 */
public class DRestClient {
	
    private String _url = null;
    private int responseCode = 0;    
    private String _message = null;
    private String _response = null;
    private JSONObject _jsonObject = null;

	public enum RequestMethod 
	{
	  	GET, POST, PUT, DELETE
	}
	
	public String getResponse() 
    {
        return _response;
    }

    public String getErrorMessage() 
    {
        return _message;
    }

    public int getResponseCode() 
    {
        return responseCode;
    }

    public DRestClient(String baseUrlString, JSONObject jsonObject) {
    	this._url = baseUrlString;
    	this._jsonObject = jsonObject;
	}
    
    
    public void Execute(RequestMethod method) throws IOException
    {
        switch(method)
        {
            case GET:
            {
                HttpGet requestGet = new HttpGet(_url);
                
                //sets a request header so the page receiving the request will know what to do with it
                requestGet.setHeader("Accept", "application/json");
                requestGet.setHeader("Content-type", "application/json");
                
                executeRequest(requestGet, _url);
                break;
            }
            case POST:
            {
                HttpPost requestPost = new HttpPost(_url);
                
                //sets a request header so the page receiving the request will know what to do with it
                requestPost.setHeader("Accept", "application/json");
                requestPost.setHeader("Content-Type", "application/json");
                requestPost.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36");
                //requestPost.setHeader("Accept-Encoding", "gzip");
                
                if( _jsonObject != null)				 
                {
                	//passes the results to a string builder/entity
                    StringEntity se = new StringEntity(_jsonObject.toString());

                    //sets the post request as the resulting string
                    requestPost.setEntity(se);
                }
              //Handles what is returned from the page 
                executeRequest(requestPost, _url);
                break;
            }
            case PUT:
            {
            	HttpPut requestPut = new HttpPut(_url);
            	requestPut.setHeader("Accept", "application/json");
            	requestPut.setHeader("Content-type", "application/json");
            	 
            	/*if( jsonObject != null)				 
                {
                	//passes the results to a string builder/entity
                    StringEntity se = new StringEntity(jsonObject.toString());

                    //sets the post request as the resulting string
                    requestPut.setEntity(se);
                }*/
            	executeRequest(requestPut, _url);
            	break;
            }
        }
    }
    
    private void executeRequest(HttpUriRequest request, String url) throws IOException
    {
    	HttpClient client = null;
    	client = new DefaultHttpClient();
    	
        client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);
        ConnManagerParams.setTimeout(client.getParams(), 30000);
        HttpResponse httpResponse = client.execute(request);
        responseCode = httpResponse.getStatusLine().getStatusCode();
        _message = httpResponse.getStatusLine().getReasonPhrase();
        
        if(responseCode == 200)
        {
        	HttpEntity entity = httpResponse.getEntity();
        	if (entity != null)
        	{
        		InputStream instream = entity.getContent();
        		_response = convertStreamToString(instream);
        		instream.close();
        	}
        }
    }
    

    private static String convertStreamToString(InputStream is) 
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}