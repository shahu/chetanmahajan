/**
 * 
 */
package com.truespider.gurukul.adapter;

import java.util.List;

import org.json.JSONObject;

import com.truespider.gurukul.R;
import com.truespider.gurukul.model.DConstants;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DAccuracyAdapter extends ArrayAdapter<JSONObject> {

	private List<JSONObject> _items = null;
	private Context _context = null;
	
	private class ViewHolder {
		TextView txtId;
        TextView txtTitle;
        TextView txtAddress;
        TextView txtPhone;
        TextView txtStateExam;
        TextView txtMarks;
    }
	
	public DAccuracyAdapter(Context context, int resource, List<JSONObject> items) {
		super(context, resource, items);
		this._context = context;
		this._items = items;
	}
	
	@Override
	public JSONObject getItem(int position) {
		return _items.get(position);
	}
	
	
	/**
	 * List row layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		// get a reference to the object
		JSONObject accuracyJsonObject = getItem(position);

		LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		// if it's not already created
		if (convertView == null) {
			convertView = vi.inflate(R.layout.layout_accuracy_list_item, null);
			
			holder = new ViewHolder();
			holder.txtId = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_id);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_title);
            holder.txtAddress = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_address);
            holder.txtPhone = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_phone);
            holder.txtStateExam = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_state);
            holder.txtMarks = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_marks);
           
            
            convertView.setTag(holder);
		}
		// it's already created, reuse it
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		 

		// get the text fields for the row
		holder.txtId.setText((position + 1) + ".");
		holder.txtTitle.setText("" + accuracyJsonObject.optString(DConstants.D_ACCURACY_NAME));
		holder.txtAddress.setText("" + accuracyJsonObject.optString(DConstants.D_ACCURACY_COLLAGE_NAME));
		holder.txtPhone.setText("PH: " + accuracyJsonObject.optString(DConstants.D_ACCURACY_PHONE));
		holder.txtStateExam.setText("State Exam: SML : " + accuracyJsonObject.optString(DConstants.D_ACCURACY_SML)
				+ " MH-CET : " + accuracyJsonObject.optString(DConstants.D_ACCURACY_MHCET));
		holder.txtMarks.setText("Deeper Kausauti: DML : " + accuracyJsonObject.optString(DConstants.D_ACCURACY_DML)
				+ " Marks : " + accuracyJsonObject.optString(DConstants.D_ACCURACY_MARKS));

		return convertView;
	}
}
