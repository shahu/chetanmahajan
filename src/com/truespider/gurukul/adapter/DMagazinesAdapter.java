/**
 * 
 */
package com.truespider.gurukul.adapter;

import java.util.List;

import org.json.JSONObject;

import com.truespider.gurukul.R;
import com.truespider.gurukul.model.DConstants;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DMagazinesAdapter extends ArrayAdapter<String> {


	private String[] _items = null;
	private Context _context = null;
	
	private class ViewHolder {
        TextView txtTitle;
    }
	
	public DMagazinesAdapter(Context context, int resource, String[] items) {
		super(context, resource, items);
		this._context = context;
		this._items = items;
	}
	
	@Override
	public String getItem(int position) {
		return _items[position];
	}
	
	
	/**
	 * List row layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		// get a reference to the object

		LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		// if it's not already created
		if (convertView == null) {
			convertView = vi.inflate(R.layout.layout_magazines_list_item, null);
			
			holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_magazines_list_item_title);
            
            convertView.setTag(holder);
		}
		// it's already created, reuse it
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtTitle.setText("" + getItem(position));

		return convertView;
	}
}
