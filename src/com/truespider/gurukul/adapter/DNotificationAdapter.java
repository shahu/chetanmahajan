/**
 * 
 */
package com.truespider.gurukul.adapter;

import java.util.ArrayList;

import org.json.JSONObject;

import com.truespider.gurukul.R;
import com.truespider.gurukul.model.DConstants;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DNotificationAdapter extends ArrayAdapter<JSONObject> {
	
	private ArrayList<JSONObject> _notificationLists = null;
	private Context _context = null;
	
	private class ViewHolder {
        TextView txtMessage;
        TextView txtDate;
    }

	public DNotificationAdapter(Context context, int resource, ArrayList<JSONObject> jsonNotificationsList) {
		super(context, resource, jsonNotificationsList);
		this._notificationLists = jsonNotificationsList;
		this._context = context;
	}

	@Override
	public JSONObject getItem(int position) {
		return _notificationLists.get(position);
	}
	

	/**
	 * List row layout
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		// get a reference to the object
		JSONObject notificationJsonObject = getItem(position);

		LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		// if it's not already created
		if (convertView == null) {
			convertView = vi.inflate(R.layout.layout_notification_list_item, null);
			
			holder = new ViewHolder();
			holder.txtMessage = (TextView) convertView.findViewById(R.id.txt_notification_list_item_message);
            //holder.txtDate = (TextView) convertView.findViewById(R.id.txt_accuracy_list_item_title);
           
            
            convertView.setTag(holder);
		}
		// it's already created, reuse it
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		 

		// get the text fields for the row
		holder.txtMessage.setText("" + notificationJsonObject.optString("message"));
		//holder.txtDate.setText("" + accuracyJsonObject.optString(DConstants.D_ACCURACY_COLLAGE_NAME));

		return convertView;
	}
}
