/**
 * 
 */
package com.truespider.gurukul.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.truespider.gurukul.R;

/**
 * @author HP
 *
 */
public class DGridAdapter extends BaseAdapter {

	private Context _context = null;
    private String[] _items = null;
    private int[] _imageIds = null;
    public DGridAdapter(Context context, String[] items, int[] imageIds) {
    	_context = context;
    	this._items = items;
    	this._imageIds = imageIds;
    }
      
    @Override
    public int getCount() {
      return _items.length;
    }
    
    @Override
    public Object getItem(int position) {
      return null;
    }
    
    @Override
    public long getItemId(int position) {
      return 0;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View view = null;
      LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      if (convertView == null) {
    	  view = new View(_context);
    	  view = inflater.inflate(R.layout.layout_home_grid_item, null);
    	  //TextView textView = (TextView) view.findViewById(R.id.txt_home_grid_item);
    	  ImageView imageView = (ImageView)view.findViewById(R.id.img_title_logo);
    	  //textView.setText(_items[position]);
    	  imageView.setImageResource(_imageIds[position]);
      } else {
    	  view = (View) convertView;
      }
      return view;
    }
}