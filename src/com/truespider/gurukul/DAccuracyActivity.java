/**
 * 
 */
package com.truespider.gurukul;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.truespider.gurukul.R;
import com.truespider.gurukul.adapter.DAccuracyAdapter;

/**
 * @author HP
 *
 */
public class DAccuracyActivity extends Activity {

	private DAccuracyAdapter _adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_accuracy);
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Deeper Accuracy");
		getActionBar().setCustomView(mCustomView);
		
		
		ListView listView = (ListView) findViewById(R.id.list_accuracy);
		_adapter = new DAccuracyAdapter(getApplicationContext(), R.layout.layout_accuracy_list_item, new ArrayList<JSONObject>());
		listView.setAdapter(_adapter);
		
		setAccuracyData();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}

	
	private String getDataFromAsset() {
		try {
		    InputStream input = getAssets().open("Accuracy.json");

		    // do reading, usually loop until end of file reading  
		    int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            // byte buffer into a string
            return new String(buffer);
		} catch (IOException e) {
		    //log the exception
		}
		return null;
	}
	
	
	
	
	private void setAccuracyData() {
		String jsonData = getDataFromAsset();
		try {
			JSONArray jsonAccuracyArray = new JSONArray(jsonData);
			if(jsonAccuracyArray != null && jsonAccuracyArray.length() > 0) {
				int accuracyCount = jsonAccuracyArray.length();
				for(int accuracyIndex = 0; accuracyIndex < accuracyCount; accuracyIndex++) {
					JSONObject jsonAccuracy = jsonAccuracyArray.getJSONObject(accuracyIndex);
					_adapter.add(jsonAccuracy);
				}
			}
		} catch (JSONException e) {
		}
	}	
}