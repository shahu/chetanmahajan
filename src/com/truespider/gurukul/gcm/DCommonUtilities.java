package com.truespider.gurukul.gcm;


import android.content.Context;
import android.content.Intent;

import com.truespider.gurukul.model.DConstants;

public final class DCommonUtilities {
	
	 

    /**
     * Tag used on log messages.
     */
    static final String TAG = "Android GCM";

    public static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    public static void displayMessage(Context context, String message) 
    {
        Intent intent = new Intent(DConstants.DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
