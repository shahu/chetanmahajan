package com.truespider.gurukul.gcm;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.truespider.gurukul.R;
import com.truespider.gurukul.service.DRestClient;
import com.truespider.gurukul.service.DRestClient.RequestMethod;
import com.truespider.gurukul.model.DConstants;

public final class DServerUtilities 
{
	private static final int MAX_ATTEMPTS = 5;
	public static int _statusCode = 0; 
    /**
     * Register this account/device pair within the server.
     *
     */
    public static void register(Context context, String userId, String deviceToken)
    {
        //Log.i("TMGCMIntentService", "registering device (deviceToken = " + deviceToken + ")");
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("Accept", "application/json"));
        nameValuePairs.add(new BasicNameValuePair("Content-type", "application/json"));
         
        
        // long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register on our server
        // As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS;) 
        {
        	try 
			{
	            //Log.d(TAG, "Attempt #" + i + " to register");
	            //displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
	           
	            JSONObject userDataJsonObject = new JSONObject();
	            userDataJsonObject.put(DConstants.D_PUSH_NOTIFICATION_USER_ID, userId);
	            userDataJsonObject.put(DConstants.D_PUSH_NOTIFICATION_REGISTRATION_ID, deviceToken);
	            Log.i("deviceToken==>", deviceToken);
	            JSONObject userJsonObject = new JSONObject();
	            userJsonObject.putOpt(DConstants.D_LOGIN_USER, userDataJsonObject);
	            DRestClient restClient = new DRestClient(DConstants.D_BASE_PUSH_REGISTRATION_URL, userJsonObject);
	            restClient.Execute(RequestMethod.POST);
	            String response = restClient.getResponse();
	            
				if(_statusCode != 401)
				{
					if(response.indexOf("HTTP_USER_AGENT") != -1)
						response = response.substring(response.indexOf("{"));
					 JSONObject jsonObject = new JSONObject(response);
					if(jsonObject == null || jsonObject.length() < 0)
					{
						//String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
						//TMCommonUtilities.displayMessage(context, message);
					}
					else
					{
						GCMRegistrar.setRegisteredOnServer(context, true);
						 
						int result = jsonObject.optInt("error");
						//context.getSharedPreferences("TRINTME", 0).edit().putLong("DEVICE_ID", jsonObject.getLong("deviceId")).commit();
						if(result == 0)
						{
							/*String msg = "" + jsonObject.getInt("UserId");*/
							//CommonUtilities.displayMessage(context, "User id is : ");
							//TMCommonUtilities.displayMessage(context, "Successfully register on server");
						}
					}
				}
				return;
	        }
	    	catch(Exception e)
	    	{
	    		
	    	}
        }
//        String message = context.getString(R.string.server_register_error, MAX_ATTEMPTS);
//        CommonUtilities.displayMessage(context, message);
    }

    /**
     * Unregister this account/device pair within the server.
     */
    public static void unregister(Context context, String userId) {
    	
    	 try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			 nameValuePairs.add(new BasicNameValuePair("token", context.getSharedPreferences("gpsPrefs", 0).getString("TOKEN", null)));
			 nameValuePairs.add(new BasicNameValuePair("Accept", "application/json"));
			 nameValuePairs.add(new BasicNameValuePair("Content-type", "application/json"));
			 DRestClient restClient = new DRestClient("", null);
			 restClient.Execute(RequestMethod.POST);
			 //String response = restClient.getResponse();
			 _statusCode = restClient.getResponseCode();
			
			//JSONObject jsonObject = new JSONObject(response);
			if(_statusCode != 401)
			{
				 //Log.i("", jsonObject.toString());
				 GCMRegistrar.setRegisteredOnServer(context, false);
			     String message = context.getString(R.string.server_unregistered);
			     DCommonUtilities.displayMessage(context, message);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}/* catch (JSONException e) {
			e.printStackTrace();
		}*/
    }
    
    
    

    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(String endpoint, Map<String, String> params) throws IOException 
    {   	
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) 
        {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
            if (iterator.hasNext()) 
            {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        //Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
        	//Log.e("URL", "> " + url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            
            //InputStream instream = conn.getInputStream();
            
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
            	throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}