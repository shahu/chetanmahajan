/**
 * 
 */
package com.truespider.gurukul;

import org.json.JSONException;
import org.json.JSONObject;

import com.truespider.gurukul.R;
import com.truespider.gurukul.controller.DApplication;
import com.truespider.gurukul.service.DServiceTask;
import com.truespider.gurukul.service.DRestClient.RequestMethod;
import com.truespider.gurukul.model.DConstants;
import com.truespider.gurukul.model.DServiceNotification;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * @author HP
 *
 */
public class DLoginActivity extends DBaseActivity implements DServiceNotification{

	private EditText _edtUserName = null;
	private EditText _edtPassword = null;
	private ProgressBar _progressBar = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		_edtUserName = (EditText) findViewById(R.id.edt_login_username);
		_edtPassword = (EditText) findViewById(R.id.edt_login_password);
		
		_progressBar = (ProgressBar) findViewById(R.id.progressbar_login);
		
		((Button) findViewById(R.id.btn_login_submit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loginRequest();
			}
		});
		
		((Button) findViewById(R.id.btn_signup_submit)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DLoginActivity.this, DSignUpActivity.class));
			}
		});
	}
	
	
	private boolean checkFieldValidation() {
		if(_edtUserName.getText().toString().length() == 0)
		{
			_edtUserName.setError("Please enter valid username");
			return false;
		}
		else if(_edtPassword.getText().toString().length() == 0)
		{
			_edtPassword.setError("Please enter valid password");
			return false;
		}
		return true;
	}
	
	
	private void loginRequest() {
		if(checkFieldValidation())
		{
			if(DApplication.getSingletonObj().isInternetAvailable())
			{
				try {
				_progressBar.setVisibility(View.VISIBLE);
				
				JSONObject jsonObject = new JSONObject();
				jsonObject.put(DConstants.D_LOGIN_EMAIL, _edtUserName.getText());
				jsonObject.put(DConstants.D_LOGIN_PASSWORD, _edtPassword.getText());
				JSONObject object = new JSONObject();
				object.putOpt(DConstants.D_LOGIN_USER, jsonObject);
				DServiceTask serviceTask = new DServiceTask(this, RequestMethod.POST, object);
				serviceTask.execute(new String[]{DConstants.D_BASE_URL });
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
			else
			{
				showToast("Please check internet connection");
			}
		}
	}
	
	
	private void showHomeActivity() {
		startActivity(new Intent(DLoginActivity.this, DHomeActivity.class));
		finish();
	}

	
	private void showToast(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void serviceNotification(int statusCode, String response) {
		if(statusCode == 200){
			try {
				JSONObject jsonObject = new JSONObject(response);
				if(jsonObject.optBoolean(DConstants.D_SUCCESS))
				{
					JSONObject userJsonObject = jsonObject.optJSONObject(DConstants.D_LOGIN_DATA).
						optJSONObject(DConstants.D_LOGIN_USER);
					getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0).edit().
						putString(DConstants.D_SHARED_USER_ID, "" + userJsonObject.optString(DConstants.D_ACCURACY_ID)).commit();
					getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0).edit().
					putString(DConstants.D_SHARED_USER_EMAIL, "" + userJsonObject.optString(DConstants.D_SHARED_USER_EMAIL)).commit();
					showToast(jsonObject.optString(DConstants.D_INFO));
					Log.i("Check user", "=====================");
					showHomeActivity();
				}
				else
				{
					Log.i("Check user", "********************************");
					showToast(jsonObject.optString(DConstants.D_INFO));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}			
		}
		else
		{
		
		}
		_progressBar.setVisibility(View.GONE);
		showToast("Please Check UserName & Password");
	}
}