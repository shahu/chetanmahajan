/**
 * 
 */
package com.truespider.gurukul;

import com.truespider.gurukul.R;
import com.truespider.gurukul.controller.DApplication;
import com.truespider.gurukul.model.DConstants;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

/**
 * @author HP
 *
 */
public class DSplashActivity extends DBaseActivity {

	private Handler _handler = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		//_handler = new Handler();
		//_handler.postDelayed(splashRunnable, 2000);
		initialializeDatabase();
	}
	
	protected void onStop() {
		super.onStop();
		if(_handler != null)
		{
			_handler.removeCallbacks(splashRunnable);
			_handler = null;
		}
	};
	
	private boolean isAutherizedUser() {
		if(getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0).getString(DConstants.D_SHARED_USER_ID, null) == null)
			return false;
		return true;
	}
	
	Runnable splashRunnable = new Runnable() {
		
		@Override
		public void run() {
			showActivity();
		}
	};
	
	
	/**
	 * 
	 */
	private void initialializeDatabase() {
		if(!DApplication.getDBInstance().checkDataBase())
			new IntializeDatabase().execute();
		else {
			DApplication.getDBInstance().openDatabase();
			showActivity();
		}
	}
	
	
	private void showActivity() {
		if(isAutherizedUser())
		{
			startActivity(new Intent(DSplashActivity.this, DHomeActivity.class));
			finish();
		}
		else
		{
			startActivity(new Intent(DSplashActivity.this, DLoginActivity.class));
			finish();
		}
	}

	
	class IntializeDatabase extends AsyncTask<String, String, Boolean>
	{
		@Override
		protected Boolean doInBackground(String... params) {
			DApplication.getDBInstance().createDataBase();
			DApplication.getDBInstance().openDatabase();
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			showActivity();
		}
	}
}