/**
 * 
 */
package com.truespider.gurukul;



import java.util.ArrayList;
import java.util.List;

import com.truespider.gurukul.R;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;


/**
 * @author HP
 *
 */
public class DPalakActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_palak);
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Tumhi Amhi Palak");
		getActionBar().setCustomView(mCustomView);
		
		
		WebView webview = (WebView) findViewById(R.id.webView_palak);
		webview.setWebChromeClient(new WebChromeClient(){
            @SuppressWarnings("deprecation")
			@Override
            public void onExceededDatabaseQuota(String url, String
                    databaseIdentifier, long currentQuota, long estimatedSize, long
                                                        totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(estimatedSize * 2);
            }
            
            
        });
        //content.addJavascriptInterface(new WebAppInterface(this), "Jasandra");
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(false);
        // Jellybean rightfully tried to lock this down. Too bad they didn't give us a whitelist
        // while we do this
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
            Level16Apis.enableUniversalAccess(settings);
       
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.setWebViewClient(new MyWebViewClient()); 
        webview.loadUrl("file:///android_asset/TumhiAmhiPalak .html");
	}
	
	@TargetApi(16)
    private static class Level16Apis {
        static void enableUniversalAccess(WebSettings settings) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
    }
	
	 private List<String> urls = new ArrayList<String>();
	 private class MyWebViewClient extends WebViewClient {
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        if (Uri.parse(url).getHost().equals("file:///android_asset/TumhiAmhiPalak .html")) {
		            // This is my web site, so do not override; let my WebView load the page
		            return false;
		        }
		        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
		        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		        startActivity(intent);
		        return true;
		    }
		    
		    @Override
		    public void onPageFinished(WebView view, String url) {
		    	 urls.add(0, url);
		    }
		    
		    
		    
		    @Override
		    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
		    	 resend.sendToTarget();
		    }
		}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
}
