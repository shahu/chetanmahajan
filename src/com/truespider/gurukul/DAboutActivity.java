/**
 * 
 */
package com.truespider.gurukul;

import com.truespider.gurukul.R;
import com.truespider.gurukul.fragments.DAboutFragment;
import com.truespider.gurukul.fragments.DContactFragment;
import com.truespider.gurukul.fragments.DCouncilFragment;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DAboutActivity extends FragmentActivity {

	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_about);
		
		/*getActionBar().setTitle("About");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayUseLogoEnabled(false);*/
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("About");
		getActionBar().setCustomView(mCustomView);

		
		ViewPager viewPager = (ViewPager) findViewById(R.id.pager_about);
		viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
	
	
	class SectionsPagerAdapter extends FragmentStatePagerAdapter {
		String[] _tabs = null;
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			_tabs = getResources().getStringArray(R.array.str_about_tab_array);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;

			switch(position)
			{
				case 0: 
					fragment = new DAboutFragment();
					break;
				
				case 1: 
					fragment = new DCouncilFragment();
					break;
					
				case 2:
					fragment = new DContactFragment();
					break;
				
				default:
					break;
			}
			 
			return fragment;
		}

		@Override
		public int getCount() {
			// Show total pages.
			return _tabs.length;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return _tabs[position];
		}
	}
}