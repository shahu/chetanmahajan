/**
 * 
 */
package com.truespider.gurukul.model;

/**
 * @author HP
 *
 */
public class DAccuracy {
	
	private int _id = 0;
	private String _name = null;
	private String _collageName = null;
	private String _photo = null;
	private String _phone = null;
	private String _sml = null;
	private String _mhcet = null;
	private String _dml = null;
	private int _marks = 0;
	private String _options = null;
	private String _url = null;
	/**
	 * @return the _id
	 */
	public int get_id() {
		return _id;
	}
	/**
	 * @param _id the _id to set
	 */
	public void set_id(int _id) {
		this._id = _id;
	}
	/**
	 * @return the _name
	 */
	public String get_name() {
		return _name;
	}
	/**
	 * @param _name the _name to set
	 */
	public void set_name(String _name) {
		this._name = _name;
	}
	/**
	 * @return the _collageName
	 */
	public String get_collageName() {
		return _collageName;
	}
	/**
	 * @param _collageName the _collageName to set
	 */
	public void set_collageName(String _collageName) {
		this._collageName = _collageName;
	}
	/**
	 * @return the _photo
	 */
	public String get_photo() {
		return _photo;
	}
	/**
	 * @param _photo the _photo to set
	 */
	public void set_photo(String _photo) {
		this._photo = _photo;
	}
	/**
	 * @return the _phone
	 */
	public String get_phone() {
		return _phone;
	}
	/**
	 * @param _phone the _phone to set
	 */
	public void set_phone(String _phone) {
		this._phone = _phone;
	}
	/**
	 * @return the _sml
	 */
	public String get_sml() {
		return _sml;
	}
	/**
	 * @param _sml the _sml to set
	 */
	public void set_sml(String _sml) {
		this._sml = _sml;
	}
	/**
	 * @return the _mhcet
	 */
	public String get_mhcet() {
		return _mhcet;
	}
	/**
	 * @param _mhcet the _mhcet to set
	 */
	public void set_mhcet(String _mhcet) {
		this._mhcet = _mhcet;
	}
	/**
	 * @return the _dml
	 */
	public String get_dml() {
		return _dml;
	}
	/**
	 * @param _dml the _dml to set
	 */
	public void set_dml(String _dml) {
		this._dml = _dml;
	}
	/**
	 * @return the _marks
	 */
	public int get_marks() {
		return _marks;
	}
	/**
	 * @param _marks the _marks to set
	 */
	public void set_marks(int _marks) {
		this._marks = _marks;
	}
	/**
	 * @return the _options
	 */
	public String get_options() {
		return _options;
	}
	/**
	 * @param _options the _options to set
	 */
	public void set_options(String _options) {
		this._options = _options;
	}
	/**
	 * @return the _url
	 */
	public String get_url() {
		return _url;
	}
	/**
	 * @param _url the _url to set
	 */
	public void set_url(String _url) {
		this._url = _url;
	}	
}