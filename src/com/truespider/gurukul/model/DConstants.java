/**
 * 
 */
package com.truespider.gurukul.model;

/**
 * @author HP
 *
 */
public class DConstants {

	
	//public static final String D_BASE_URL = "http://truemac.in/api/v1/sessions";
	public static final String D_BASE_URL = "http://truemac.in/api/v1/sessions";
	public static final String D_BASE_PUSH_REGISTRATION_URL = "http://truemac.in/api/v1/android/get_users_data";
	public static final String D_BASE_PUSH_NOTIFICATION_URL = "http://truemac.in/api/v1/android/get_notifications";
	
	// Push notification
	public static final String SENDER_ID = "41319311200";
	public static final String DISPLAY_MESSAGE_ACTION = "com.truespider.dipper.DISPLAY_MESSAGE";
	
	// SharedPreferences constants
	public static final String D_SHARED_PREFERENCES = "DIPPER";
	//public static final String D_USER_TOKEN = "user_token";
	public static final String D_SHARED_USER_ID = "id";
	public static final String D_SHARED_USER_EMAIL = "email";
	public static final String D_USER_GCM_REGISTRATION_ID = "registration_id";
	 
	
	
	// JSON constants
	public static final String D_LOGIN_DATA = "data";
	public static final String D_LOGIN_USER = "user";
	public static final String D_LOGIN_EMAIL = "email";
	public static final String D_LOGIN_PASSWORD = "password";
	public static final String D_SUCCESS = "success";
	public static final String D_INFO = "info";
	public static final String D_PUSH_NOTIFICATION_USER_ID = "user_id";
	public static final String D_PUSH_NOTIFICATION_DEVICE_ID = "device_id";
	public static final String D_PUSH_NOTIFICATION_REGISTRATION_ID = "registration_id";
	
	// Accuracy constants
	public static final String D_ACCURACY_ID = "id";
	public static final String D_ACCURACY_NAME = "name";
	public static final String D_ACCURACY_COLLAGE_NAME = "college_name";
	public static final String D_ACCURACY_PHOTO = "photo";
	public static final String D_ACCURACY_PHONE = "phone";
	public static final String D_ACCURACY_SML = "sml";
	public static final String D_ACCURACY_MHCET = "mhcet";
	public static final String D_ACCURACY_DML = "dml";
	public static final String D_ACCURACY_MARKS = "marks";
	public static final String D_ACCURACY_OPINION = "opinion";
	public static final String D_ACCURACY_URL = "url";
	
}

