package com.truespider.gurukul.model;

public interface DServiceNotification {

	/**
	 * 
	 * @param statusCode
	 * @param response
	 */
	public void serviceNotification(int statusCode, String response);
}
