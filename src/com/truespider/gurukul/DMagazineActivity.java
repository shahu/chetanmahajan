/**
 * 
 */
package com.truespider.gurukul;


import com.truespider.gurukul.R;
import com.truespider.gurukul.adapter.DMagazinesAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

/**
 * @author HP
 *
 */
public class DMagazineActivity extends Activity {

	private DMagazinesAdapter _adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_magazines);
		
		getActionBar().setTitle("Magazines");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		String[] magazines = getResources().getStringArray(R.array.str_magazines_array);
		
		ListView listView = (ListView) findViewById(R.id.list_magazines);
		_adapter = new DMagazinesAdapter(getApplicationContext(), R.layout.layout_magazines_list_item, magazines);
		listView.setAdapter(_adapter);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
}
