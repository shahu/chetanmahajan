/**
 * 
 */
package com.truespider.gurukul;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.truespider.gurukul.R;
import com.truespider.gurukul.adapter.DNotificationAdapter;
import com.truespider.gurukul.service.DServiceTask;
import com.truespider.gurukul.service.DRestClient.RequestMethod;
import com.truespider.gurukul.model.DConstants;
import com.truespider.gurukul.model.DServiceNotification;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author HP
 *
 */
public class DNotificationActivity extends Activity implements DServiceNotification{
	
	private ProgressBar _progressbar = null;
	private DNotificationAdapter _adapter = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Notifications");
		getActionBar().setCustomView(mCustomView);
		
		_progressbar = (ProgressBar) findViewById(R.id.progressbar_notification);
		
		_adapter = new DNotificationAdapter(getApplicationContext(),
				R.layout.layout_notification_list_item, new ArrayList<JSONObject>());
		
		ListView notificationList = (ListView) findViewById(R.id.list_notification);
		notificationList.setAdapter(_adapter);
		
		getNotification();
	}

	private void getNotification() {
		_progressbar.setVisibility(View.VISIBLE);
		DServiceTask serviceTask = new DServiceTask(this, RequestMethod.GET, null);
		serviceTask.execute(new String[]{ DConstants.D_BASE_PUSH_NOTIFICATION_URL});
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    switch (menuItem.getItemId()) {
		    case android.R.id.home:
		    {
		    	finish();
		    }
	    }
	    return (super.onOptionsItemSelected(menuItem));
	}
	
	


	@Override
	public void serviceNotification(int statusCode, String response) {
		if(_progressbar != null) {
			_progressbar.setVisibility(View.GONE);
			if(statusCode == 200) {
				try {
					Log.i("", "" + response);
					JSONObject jsonData = new JSONObject(response);
					if(jsonData != null && jsonData.optBoolean(DConstants.D_SUCCESS)) {
						JSONArray notificationJSONArray = jsonData.optJSONArray(DConstants.D_LOGIN_USER);
						_adapter.clear();
						int notificationCount = jsonData.optInt("count");
						for(int notificationIndex = 0; notificationIndex < notificationCount; notificationIndex++) {
							_adapter.add(notificationJSONArray.optJSONObject(notificationIndex));
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			else {
				Log.i("", "" + response);
			}
		}
	}
}