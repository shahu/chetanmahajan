package com.truespider.gurukul;

import java.util.ArrayList;
import java.util.List;


import com.truespider.gurukul.R;
import com.truespider.gurukul.model.DConstants;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class GNotificationActivity extends Activity {

private WebView _content = null;
	
	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getActionBar().hide();
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Notification");
		getActionBar().setCustomView(mCustomView);
		setContentView(R.layout.activity_gnotification);
		
		_content = (WebView) findViewById(R.id.webView_gnotification);
		_content.setWebChromeClient(new WebChromeClient(){
            @SuppressWarnings("deprecation")
			@Override
            public void onExceededDatabaseQuota(String url, String
                    databaseIdentifier, long currentQuota, long estimatedSize, long
                                                        totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(estimatedSize * 2);
            }
            
            
        });
        //content.addJavascriptInterface(new WebAppInterface(this), "Jasandra");
        WebSettings settings = _content.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(false);
        // Jellybean rightfully tried to lock this down. Too bad they didn't give us a whitelist
        // while we do this
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
            Level16Apis.enableUniversalAccess(settings);
        // Enable database
        // We keep this disabled because we use or shim to get around DOM_EXCEPTION_ERROR_16
        //String databasePath = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        //String databasePath = "/data/data/com.truespider.dipper/databases/exam";
        SharedPreferences preferences = getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0);
		String user_id = preferences.getString(DConstants.D_SHARED_USER_ID, "-1");
		String emailId = preferences.getString(DConstants.D_SHARED_USER_EMAIL, "-1");
		Log.i(" P Email Id ===>", emailId);
        settings.setDatabaseEnabled(true);
        //settings.setDatabasePath(databasePath);
        settings.setDatabasePath("/data/data/"+this.getPackageName()+"/databases/");
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        _content.setWebViewClient(new MyWebViewClient());
        //Testing 
        //_content.loadUrl("file:///android_asset/practice.html?user_id="+user_id+"&email="+emailId);
        //Quick Fix For Url with parameters Loading Issue in WebView of 4.0.4 
        Log.i(" Webview Called For Version=>", ""+android.os.Build.VERSION.SDK_INT);
          if(android.os.Build.VERSION.SDK_INT == 14 || android.os.Build.VERSION.SDK_INT == 15)
          {
        	  _content.loadUrl("file:///android_asset/gnotification.html");
          }
          else
          {
        	  _content.loadUrl("file:///android_asset/gnotification.html?user_id="+user_id+"&email="+emailId);  
          }
		
	}
	
	
	@TargetApi(16)
    private static class Level16Apis {
        static void enableUniversalAccess(WebSettings settings) {
        	Log.i("enableUniversalAccess========>", "");
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
    }
	
	 
	 private List<String> urls = new ArrayList<String>();
	 private class MyWebViewClient extends WebViewClient {
		 
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		    	Log.i("MyWebViewClient called !!! shouldOverrideUrlLoading", url);
		        if (Uri.parse(url).getHost().equals("file:///android_asset/gnotification.html")) {
		           	Log.i(">>>>my web page", url);
		            // This is my web site, so do not override; let my WebView load the page
		            return false;
		        }
		    	Log.i(">>>>Else Other web page", url);
		        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
		        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		        startActivity(intent);
		        return true;
		    }
		    
		    @Override
		    public void onPageFinished(WebView view, String url) {
		    	Log.i("onPageFinished====>", url);
		    	 urls.add(0, url);
		    }
		    
		    
		    
		    @Override
		    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
		    	 resend.sendToTarget();
		    }
		}

}
