/**
 * 
 */
package com.truespider.gurukul;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

/**
 * @author HP
 *
 */
public class DBaseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
	}
}
