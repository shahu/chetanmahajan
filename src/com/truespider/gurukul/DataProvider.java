package com.truespider.gurukul;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataProvider {
	public static HashMap<String, List<String>> getCourseInfo()
	{
		HashMap<String, List<String>> CourseCategory =  new HashMap<String,List<String>>();
		
		List<String> First_Year = new ArrayList<String>();
		First_Year.add("Engg. Mathematics - I (sem 1)");
		First_Year.add("Engg. Graphics - II (sem 1)");
		First_Year.add("Engg. Mathematics - II (sem 2)");
		First_Year.add("Engg. Mechanics (sem 2)");
		
		List<String> Second_Year = new ArrayList<String>();
		Second_Year.add("Engg. Mechanics - III (sem 1)");
		Second_Year.add("Signal and Systems. (sem 1)");
		Second_Year.add("Network Analysis (sem 1)");
		Second_Year.add("Engg. Mathematics - III (sem 2)");
		Second_Year.add("S. O. M. E (sem 2)");
		
		List<String> DFirst_year = new ArrayList<String>();
		DFirst_year.add("Basic Maths (Sem 1)");
		DFirst_year.add("Engg. Graphics (Sem 2)");
		DFirst_year.add("Basic Physics (Sem 2)");
		DFirst_year.add("Applied Maths (Sem 2)");
		DFirst_year.add("Applied Mechanics (Sem 2)");
		DFirst_year.add("Engg. Drawing (Sem 2)");
		
		List<String> DSecond_Year = new ArrayList<String>();
		DSecond_Year.add("Mechanics Engg. Drawing (Sem 2)");
		DSecond_Year.add("Strenth of Material (S O M) (Sem 2)");
		
		CourseCategory.put("First Year Engineering", First_Year);
		CourseCategory.put("Second Year Engineering", Second_Year);
		
		CourseCategory.put("First Year Diploma", DFirst_year);
		CourseCategory.put("Second Year Diploma", DSecond_Year);
		
		
		
		return CourseCategory;
		
	}

}
