/**
 * 
 */
package com.truespider.gurukul.controller;

import com.truespider.gurukul.db.DDatabaseHelper;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

/**
 * @author HP
 *
 */
public class DApplication extends Application {

	public static DApplication _application = null;
	private static DDatabaseHelper _dbHelper = null;
	
	@Override
	public void onCreate() {
		super.onCreate();
		_application = this;
	}
	
	public static DApplication getSingletonObj() {
		return _application;
	}
	
	public boolean isInternetAvailable() {
		ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService (Context.CONNECTIVITY_SERVICE);
		if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() &&    conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			System.out.println("Internet Connection Not Present");
			return false;
		}
	}
	
	/**
	 * Create singleton object for opening database instance 
	 * @return SQliteHelper class object
	 */
	public static DDatabaseHelper getDBInstance()
	{
		if(_dbHelper == null)
			_dbHelper = new DDatabaseHelper(_application);
		return _dbHelper;
	} 
	
}
