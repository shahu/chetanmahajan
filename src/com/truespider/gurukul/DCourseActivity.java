package com.truespider.gurukul;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.truespider.gurukul.R;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class DCourseActivity extends Activity {
	HashMap<String, List<String>> Course_Category;
	List<String> Course_List;
	ExpandableListView Exp_List;
	CoursesAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText("Courses");
		getActionBar().setCustomView(mCustomView);
		setContentView(R.layout.activity_dcourse);
		
		Exp_List = (ExpandableListView) findViewById(R.id.exp_list);
		Course_Category = DataProvider.getCourseInfo();
		Course_List = new ArrayList<>(Course_Category.keySet());
		adapter = new CoursesAdapter(this, Course_Category, Course_List);
		Exp_List.setAdapter(adapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dcourse, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
