/**
 * 
 */
package com.truespider.gurukul.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author HP
 *
 */
public class DDatabaseHelper extends SQLiteOpenHelper {

	private SQLiteDatabase _database = null;
	private static String DB_NAME = "exam.db";
	private static int DB_VERSION = 1;
	private String DB_PATH = "/data/data/com.truespider.dipper/databases/";
	private Context _context = null;
	
	public DDatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		_context = context;
	}


	@Override
	public void onCreate(SQLiteDatabase arg0) {

	}

 
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}
	
	public void createDataBase()
    {
		this.getReadableDatabase();
		 
		copyDataBase();
    }
    
   /* public void openDataBase () {
        String path = DB_PATH + DB_NAME;
        _database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
     }*/
    
    public boolean checkDataBase()
    {
        SQLiteDatabase checkDB = null;
        try
        {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
        }catch(SQLiteException e)
        {
            //database does't exist yet.
        }

        if(checkDB != null)
            checkDB.close();
        return checkDB != null ? true : false;
    }
    
    
 
    private void copyDataBase()  
    {
        try {
			//Open your local db as the input stream
			InputStream myInput = _context.getAssets().open("development.sqlite3");
			// Path to the just created empty db
			String outFileName = DB_PATH + DB_NAME;
			File file = new File(DB_PATH);
			if(!file.isDirectory())
				file.mkdir();
			//Open the empty db as the output stream
			OutputStream myOutput = new FileOutputStream(outFileName);
			//transfer bytes from the inputfile to the outputfile
 
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer))>0)
			    myOutput.write(buffer, 0, length);
 
			//Close the streams
			myOutput.flush();
			myOutput.close();
			myInput.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void openDatabase() {
    	try
        {
            String myPath = DB_PATH + DB_NAME;
            _database = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
        }catch(SQLiteException e)
        {
            //database does't exist yet.
        	Log.e("", e.getMessage());
        }
	}

}
