/**
 * 
 */
package com.truespider.gurukul;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.truespider.gurukul.R;
import com.truespider.gurukul.adapter.DGridAdapter;
import com.truespider.gurukul.gcm.DServerUtilities;
import com.truespider.gurukul.gcm.DWakeLocker;
import com.truespider.gurukul.model.DConstants;

/**
 * @author HP
 *
 */
public class DHomeActivity extends Activity {

	private GridView _gridView = null;
	private SharedPreferences _preferences = null;
	private PushNotificationTask _serviceTask = null;
	private RelativeLayout _layoutHome = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		//getActionBar().setCustomView(R.layout.layout_titlebar);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.layout_titlebar, null);
		//mCustomView.setBackgroundResource(R.drawable.deeper); //Added deeper logo
		mCustomView.findViewById(R.drawable.gurukul);
		TextView txtTitle = (TextView) mCustomView.findViewById(R.id.txt_actionbar_title);
		txtTitle.setText(getResources().getString(R.string.str_home_title));
		getActionBar().setCustomView(mCustomView);
		setContentView(R.layout.activity_home);
		_preferences = getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0);
		
		final String[] homeItems = getResources().getStringArray(R.array.str_home_items_array);
		
		int[] imageIds = {
			      
			      R.drawable.practice,
			      R.drawable.result,
			      R.drawable.notification,
			      
			     // R.drawable.other_activities,
			      //R.drawable.accuracy,
			      R.drawable.features,
			      R.drawable.courses,
			      R.drawable.about_us
			     
			  };
		
		DGridAdapter adapter = new DGridAdapter(this, homeItems, imageIds);
		_gridView =(GridView) findViewById(R.id.grid_home);
		_gridView.setAdapter(adapter);
		_gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//Toast.makeText(DHomeActivity.this, "You Clicked " + homeItems[position], Toast.LENGTH_SHORT).show();
				showSelectedActivity(position);
			}
		});
		
		_layoutHome = (RelativeLayout) findViewById(R.id.layout_home_dashboard);
		
		_serviceTask = new PushNotificationTask();
		_serviceTask.execute();
	}
	
 
    private void onCreateDialog() {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you really want to exit APP?")
            .setCancelable(false)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //Do something here
                	dialog.dismiss();
                	finish();
                }
            }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
            
            
            dialog = builder.create();
            dialog.show();
    }
	
	@Override
	public void onBackPressed() {
	    // your code.
		onCreateDialog();
	}

	 
	
	
	private  DMarqueeLayout marqueeLayout = null;
	private void startRotatingAnimation() {
		 marqueeLayout = new DMarqueeLayout(this);
		 marqueeLayout.setDuration(15000);
		// marqueeLayout.addView(_txtLatestRecommendation);
		 _layoutHome.addView(marqueeLayout);
	}
	
	
	private void showSelectedActivity(int selectedIconIndex) {
		switch (selectedIconIndex) {
			case 0: // Practice
				startActivity(new Intent(DHomeActivity.this, DPracticeActivity.class));
				break;
				
			case 1: // Analysis/Result
				startActivity(new Intent(DHomeActivity.this, DResultActivity.class));
				break;
			
			case 2: // Notification
				//startActivity(new Intent(DHomeActivity.this, DNotificationActivity.class));
				startActivity(new Intent(DHomeActivity.this, GNotificationActivity.class));
				break;
				
			case 3: // Other
				SharedPreferences settings = getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0);
				String user_id = settings.getString(DConstants.D_SHARED_USER_ID, "Not reg.");
				
				//startActivity(new Intent(DHomeActivity.this, DOtherActivity.class));
				//WebView For Features
				startActivity(new Intent(DHomeActivity.this, DFeaturesActivity.class));
				break;
				
			case 4: // Accuracy
				//startActivity(new Intent(DHomeActivity.this, DAccuracyActivity.class));
				startActivity(new Intent(DHomeActivity.this, DCourseActivity.class));
				break;
			
			case 5: // About
				startActivity(new Intent(DHomeActivity.this, DAboutActivity.class));
				break;
				
			case 6:
				startActivity(new Intent(DHomeActivity.this, DAccuracyActivity.class));
				break;
				
			case 7: 
				startActivity(new Intent(DHomeActivity.this, DAboutActivity.class));
				break;
	
			default:
				break;
		}
	}
	
	
	
	/**
	 * Push notification
	 * @author : Satish
	 * @Date   : 07-Mar-2014
	 */
	
	class PushNotificationTask extends AsyncTask<String, String, String>
	{
		@Override
		protected String doInBackground(String... params) 
		{
			//Log.e("TMHomeActivity ", "Register..");
			/***************************************GCM*****************************************/
			// Make sure the device has the proper dependencies.
			GCMRegistrar.checkDevice(DHomeActivity.this);
			
			
			// Make sure the manifest was properly set - comment out this line
			// while developing the app, then uncomment it when it's ready.
			GCMRegistrar.checkManifest(DHomeActivity.this);
			
			//registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
			// Register custom Broadcast receiver to show messages on activity
	        registerReceiver(mHandleMessageReceiver, new IntentFilter(DConstants.DISPLAY_MESSAGE_ACTION));
			
			// Get GCM registration id
			String regId = GCMRegistrar.getRegistrationId(DHomeActivity.this);
			//Log.i("TMHomeActivity", "Registration id is : " + regId);
			
			// Check if regid already presents
			if (regId.equals("")) 
			{
				// Registration is not present, register now with GCM			
				GCMRegistrar.register(DHomeActivity.this, DConstants.SENDER_ID);
			}
			else 
			{
				_preferences.edit().putString(DConstants.D_USER_GCM_REGISTRATION_ID, regId).commit();
				// Device is already registered on GCM
				if (GCMRegistrar.isRegisteredOnServer(DHomeActivity.this)) 
				{
					// Skips registration.
					//Log.i("TMHomeActivity", "Already registered with GCM");
					
					return regId;
				} 
				else {
					DServerUtilities.register(DHomeActivity.this,  _preferences.getString(DConstants.D_SHARED_USER_ID, null), regId);
				}
			}
			return regId;
		}
		
		@Override
		protected void onPostExecute(String result) 
		{
			//Log.i("TMHomeActivity", ""+result);
		}
	}
	
	@Override
	protected void onDestroy() {
		if (_serviceTask != null) {
			_serviceTask.cancel(true);
		}
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}
	
	 // Create a broadcast receiver to get message and show on screen 
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
         
        @Override
        public void onReceive(Context context, Intent intent) {
             
        	//String newMessage = intent.getExtras().getString("message");
			// Waking up mobile if it is sleeping
			DWakeLocker.acquire(getApplicationContext());
			Log.i("TMBroadcast", "" +  intent.getExtras().getString("message"));
			/**
			 * Take appropriate action on this message depending upon your app requirement
			 * For now i am just displaying it on the screen
			 * */
			
			// Showing received message
			//lblMessage.append(newMessage + "\n");			
			//Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
			//showMessage();
			// Releasing wake lock
			
			DWakeLocker.release();
        }
    };
}