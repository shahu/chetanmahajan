package com.truespider.gurukul;

import java.util.HashMap;
import java.util.List;

import com.truespider.gurukul.R;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class CoursesAdapter extends BaseExpandableListAdapter {
 
	private Context ctx;
	private HashMap<String,List<String>> Course_Category;
	private List<String> Course_list;
	
	public CoursesAdapter(Context ctx,HashMap<String,List<String>> Course_Category,List<String> Course_List )
	{
		
		this.ctx=ctx;
		this.Course_Category= Course_Category;
		this.Course_list = Course_List;
	}
	
	@Override
	public Object getChild(int parent, int child) {
		// TODO Auto-generated method stub
		Log.i("getChild", "=====================parent="+parent);
		Log.i("getChild", "=====================child="+child);
		return Course_Category.get(Course_list.get(parent)).get(child);
		//return Course_Category.get(Course_Category.get(parent)).get(child);
	}

	@Override
	public long getChildId(int parent, int child) {
		// TODO Auto-generated method stub
		return child;
	}

	@Override
	public View getChildView(int parent, int child,
			boolean lastChild, View convertview, ViewGroup parentview) {
		
		 String child_title = (String) getChild(parent, child);
		 if(convertview == null)
		 {
			 LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 convertview = inflator.inflate(R.layout.child_layout, parentview,false); 
		 }
		 TextView child_textview = (TextView) convertview.findViewById(R.id.child_txt);
		 child_textview.setText(child_title);
		 
		// TODO Auto-generated method stub
		return convertview;
	}

	@Override
	public int getChildrenCount(int arg0) {
		
		return Course_Category.get(Course_list.get(arg0)).size(); 
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return Course_list.get(arg0);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return Course_list.size();
	}

	@Override
	public long getGroupId(int agr0) {
		// TODO Auto-generated method stub
		return agr0;
	}

	@Override
	public View getGroupView(int parent, boolean isExpanded,
			View convertview, ViewGroup parentview) {
		String group_title =(String) getGroup(parent);
		if(convertview == null)
		{
			LayoutInflater  inflator =  (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertview = inflator.inflate(R.layout.parent_layout, parentview,false);
		}
		
		TextView parent_textView = (TextView) convertview.findViewById(R.id.parent_txt);
		parent_textView.setTypeface(null, Typeface.BOLD);
		parent_textView.setText(group_title);
		
		// TODO Auto-generated method stub
		return convertview;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	
}
