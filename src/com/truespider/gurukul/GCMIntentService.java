package com.truespider.gurukul;


import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import static com.truespider.gurukul.gcm.DCommonUtilities.displayMessage;

import com.google.android.gcm.GCMBaseIntentService;
import com.truespider.gurukul.R;
import com.truespider.gurukul.gcm.DServerUtilities;
import com.truespider.gurukul.service.DServiceTask;
import com.truespider.gurukul.service.DRestClient.RequestMethod;
import com.truespider.gurukul.model.DConstants;
import com.truespider.gurukul.model.DServiceNotification;

public class GCMIntentService extends GCMBaseIntentService implements DServiceNotification 
{

	private static final String TAG = "TMGCMIntentService";

    public GCMIntentService() 
    {
        super(DConstants.SENDER_ID);
    }
    

    protected String getGCMIntentServiceClassName(Context context) 
    {
        String className = context.getPackageName() +".DGCMIntentService";
        return className;
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) 
    {
         Log.i(TAG, "Device registered: regId = " + registrationId);
        //displayMessage(context, "Your device registred with GCM");
        //ServerUtilities.register(context, GMainActivity.deviceType, registrationId);
        SharedPreferences prefences = context.getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0);
        prefences.edit().putString(DConstants.D_USER_GCM_REGISTRATION_ID, registrationId).commit();
        
        DServerUtilities.register(context, prefences.getString(DConstants.D_SHARED_USER_ID, null), registrationId);
    }

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) 
    {
        //log.i(TAG, "Device unregistered");
        //displayMessage(context, getString(R.string.gcm_unregistered));
        DServerUtilities.unregister(context, ""+context.getSharedPreferences(DConstants.D_SHARED_PREFERENCES, 0).getString(
        		DConstants.D_SHARED_USER_ID, null));
    	
    	/*Log.i(TAG, "Device unregistered");
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            //ServerUtilities.customUnregister(context, registrationId); // here 3rd server side unregister
        	Log.i(TAG, "Service is registered");
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring server unregister callback");
        }*/
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) 
    {
    	try {
			//log.i(TAG, "Received message");
    		//Log.i("TMBroadcast", "" +  intent.getExtras().getString("message"));
			//log.i(TAG, message);
			 
			String message = intent.getExtras().getString("type");
			if(message != null)
				message = message + ": " + intent.getExtras().getString("message");
			else
				message = intent.getExtras().getString("message");
			 
			
			Log.i(TAG, message);
			//boolean isSoundPlay = !(intent.getExtras().getString("sound")).equals("")  ? true : false;
			displayMessage(context, message);
			// notifies user
			generateNotification(context, message, true);
			
			//getUserTrintProfile(context.getSharedPreferences("TRINTME", 0).getString("FACEBOOK_USER_ID", null));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    private void getUserTrintProfile(String userFacebookId) {
		DServiceTask serviceTask = new DServiceTask(this, RequestMethod.POST, null);
		/*String url = API_BASE_URL + "getusertrintprofile?"+TM_UID + "=" 
			+ userFacebookId + "&" + TM_API_TOKEN + "=" + TM_API_TOKEN_VALUE;
		serviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{url});*/
	}
    
    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        //log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message, false);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        //log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error, errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, boolean isSoundPlay) 
    {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        
        String title = context.getString(R.string.app_name);
        
        Intent notificationIntent = new Intent(context, DNotificationActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        
        // Play default notification sound
        if(isSoundPlay)
        	notification.defaults |= Notification.DEFAULT_SOUND;
        
        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
        
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);      
    }
 

	@Override
	public void serviceNotification(int statusCode, String response) {
		
	}


	 
}