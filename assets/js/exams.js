var time_elapsed = 0;
var rem_time = 0;
function startTimer(exam_id) {
    var _time = "";
    var hr = Math.floor(rem_time/3600); 
    var min = Math.floor((rem_time%3600)/60);
    var sec = Math.floor((rem_time%3600)%60);
    if(rem_time > 0)
    {
        _time = hr + ":"+ min + ":" + sec;
        rem_time = rem_time - 1;
        time_elapsed = time_elapsed + 1;
    }
    if (time_elapsed % 5 == 0 ) {
        html5sql.process("update exams set time_elapsed = " + time_elapsed +" where id=" + exam_id);
    };
    if(rem_time == 0)
    {
        alert("Exam Time is Over...");  
    } else {
        setTimeout("startTimer("+exam_id+")", 1000);
    }
    $("#counter").html(_time);
}

function handleAndroidBack(){
    if(!App.back()){
        Jasandra.back();
    }
} 
/*       		  
setTimeout(function(){
    App.load('exams');   
}, 1000);
*/
function startActivity(){
	App.load('exams');
}
App.controller('exams', function(page){
    console.log("sdafsa");
    html5sql.process('select * from exams',
        function(transaction, results){
            var content = '<ul class="app-list">';
            for(var i=0;i<results.rows.length;i++){
                content += '<li class="app-button" data-target="exam" data-section = '+results.rows.item(i).id+'>'+results.rows.item(i).name+'</li>';
            }
            $('#exams-content').html(content+'</ul>');
            $('#exams-content li').click(function(event){
                event.stopPropagation();
                var exam_id = $(event.target).data('section');
                App.load('exam', {exam_id: exam_id});
            });
        },
        log_sql_error);
});

App.controller('exam', function(page, exam){
    html5sql.process('select * from exams where id='+exam.exam_id,
        exam_loaded_successfully,log_sql_error);
    function exam_loaded_successfully(transaction, results) {
        $(page).find('#exam-name').html(results.rows.item(0).name);
        $(page).find('#exam-duration').html(results.rows.item(0).duration);
        examDuration = parseInt(results.rows.item(0).duration);
        if (parseInt(results.rows.item(0).time_elapsed) > 0) {
            time_elapsed = parseInt(results.rows.item(0).time_elapsed);
        };
        $(page).find('#exam-marks').html(results.rows.item(0).total_marks);
        $(page).find('#start-exam').attr("data-section", results.rows.item(0).id);    
        $(page).find('#exam-ended').html(results.rows.item(0).end);
        $(page).find('#start-exam').click(function(event){
            var exam_id = $(event.target).data('section');
            rem_time = examDuration - time_elapsed;
            startTimer(exam_id);
            App.load('exam-question', {exam_id: exam_id, index: 0});
        });
    }
});
App.controller('exam-question', function(page, exam){
    var question = null;
    var exam_question = null;
    var total_questions = null;
    html5sql.process('select * from exam_questions where exam_id='+exam.exam_id,
        load_question,log_sql_error);
    function load_question (transaction, results) {
        exam_question = results.rows.item(exam.index);
        total_questions = results.rows.length;
        html5sql.process('select * from questions where id='+exam_question.question_id,
        question_loaded_successfully,log_sql_error);
    
        function question_loaded_successfully(transaction, results) {
            question = results.rows.item(0);
            $(page).find('#question-no').html(exam.index + 1 );
            $(page).find('#question-text').html(question.text);
            $(page).find('#option-1').html(question.option_1);
            $(page).find('#option-2').html(question.option_1);
            $(page).find('#option-3').html(question.option_1);
            $(page).find('#option-4').html(question.option_1);
            MathJax.Hub.Typeset();
        }
        if (exam_question.answer_option != null) {
            $(page).find('#option-' + exam_question.answer_option).addClass("selected");
        };
    }
    $(page).on('appShow', function(){ 
        $(page).find('#previous-question').click(function(){
            if (exam.index == 0) 
                alert("First question");
            else 
                App.load('exam-question', {exam_id: exam.exam_id, index: exam.index - 1});
        });
        $(page).find('#skip-question').click(function(){
            html5sql.process("update exam_questions set status = 'skiped' where id="+exam_question.id);
            next_question();
        });
        $(page).find('#next-question').click(function(){
            var selected_option = $(page).find('.selected').data('option');
            if (typeof(selected_option) == "undefined") 
                alert("select atleast one option");
            else {
                html5sql.process("update exam_questions set status = 'visited', answer_option = "+ selected_option +" where id="+exam_question.id);
                next_question();
            }
        });
        $(page).find('.question-option p').click(function(event){
            $(page).find('.question-option p').removeClass("selected");
            $(event.target).addClass("selected");
        });
        $(page).find('#question-list-btn').click(function(event){
            App.load('question-list', {exam_id: exam.exam_id});
        });
        function next_question() {
            if (exam.index < total_questions-1)
                App.load('exam-question', {exam_id: exam.exam_id, index: exam.index + 1});
            else
                App.load('exam-question', {exam_id: exam.exam_id, index:0});
        }
    });    
});

App.controller('question-list', function(page, exam){

    html5sql.process('select * from exam_questions where exam_id='+exam.exam_id,
        load_question_list,log_sql_error);
    function load_question_list (transaction, results) {
        $(page).find('#question-list').append('<ul class="app-list">');
        for(var i=0;i<results.rows.length;i++){
             content = '<li class="app-button q_no '+ results.rows.item(i).status +'" data-index= '+ i +'> '+ (i+1) +' </li>';
            //content = '<button type="button"><li class="app-button q_no '+ results.rows.item(i).status +'" data-index= '+ i +'> '+ (i+1) +' </li> </button>';
            //content = '<li class="app-button q_no '+ results.rows.item(i).status +'" style="'+'display:inline;'+'"data-index='+i+' ><button type="button"  >'+(i+1) +'</button> </li>';

            $(page).find('#question-list').append(content);
        }
        $(page).find('#question-list').append('</ul>');
        
    }
    $(page).on('appShow', function(){ 
        $(page).find('.q_no').click(function(event){
            var question_index = $(event.target).data('index');
            App.load('exam-question', {exam_id: exam.exam_id, index:parseInt(question_index)});
        });
        $(page).find('#submit-btn').click(function(event){
            html5sql.process("update exams set end = 'true' where id=" + exam.exam_id);
            App.load('result', {exam_id: exam.exam_id});
        });
    });
});

App.controller('results', function(page){
    html5sql.process('select * from exams where end="true"',
        load_result_list,log_sql_error);
    function load_result_list (transaction, results) {
        $(page).find('#results-content').html('<ul class="app-list">');
        for(var i=0;i<results.rows.length;i++){
            content = '<li class="app-button exam" data-id= '+ results.rows.item(i).id +'>'+ results.rows.item(i).name +'</li>';
            $(page).find('#results-content').append(content);
        }
        $(page).find('#results-content').append('</ul>');
    }
    $(page).on('appShow', function(){ 
        $(page).find('.exam').click(function(event){
            var exam_id = $(event.target).data('id');
            App.load('result', {exam_id: exam_id});
        });
    });
});

App.controller('result', function(page, exam){
    var $template = $(page).find('.question').remove();
    var $questions = $(page).find('.questions');
    var corrent_qus_count = 0;
    var wrong_qus_count = 0;
    var i;

    html5sql.process('select * from exam_questions where exam_id='+exam.exam_id,
        load_result,log_sql_error);
        function load_result (transaction, results) {
            for(i=0;i<results.rows.length;i++){
                var exam_question = results.rows.item(i);
                var $question = $template.clone(true);
                $question.find('#question_no').html(i+1);
                $question.find('#your_answer').html(exam_question.answer_option);
                $question.find('#correct_answer').html(exam_question.correct_option);
                $question.find('#status').html(exam_question.status);
                $questions.append($question);
                if (exam_question.answer_option != null) {
                    if (exam_question.answer_option == exam_question.correct_option) {
                        corrent_qus_count += 1;
                        $question.find('#ans_status').html("<img src='appsets/images/correct.png'>");
                    } else {
                        wrong_qus_count += 1;
                        $question.find('#ans_status').html("<img src='appsets/images/incorrect.png'>");
                    }
                };
            }
        }

    $(page).on('appShow', function(){ 
        $(page).find('#summary_total').html(i);
        $(page).find('#summary_attempted').html(corrent_qus_count + wrong_qus_count);
        $(page).find('#summary_unattempted').html(i - (corrent_qus_count + wrong_qus_count));
        $(page).find('#summary_correct').html(corrent_qus_count);
        $(page).find('#summary_wrong').html(wrong_qus_count);
        $(page).find('#summary_marks').html(corrent_qus_count*4 - wrong_qus_count*1);
    });
});
/* Utilities */
function log_sql_error(error, statement){
  console.log("Error: "+error.message+" when processing " + statement);
}


