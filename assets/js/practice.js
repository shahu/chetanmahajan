function handleAndroidBack(){
    if(!App.back()){
        Jasandra.back();
    }
}
/*
setTimeout(function(){
    App.load('practice');   
}, 1000);
*/
function startActivity(){
	App.load('practice');
}

var questions = [],
    question_index = null,
    question = null,
    previous_index = null,
    next_index = null,
    chapter = null,
	m_chapter_id = -1;
	m_subject_name = "undefined";
    first_question = null,
    last_question = null,
    time_required = 0,
    attempts = 0,
    practice_timer = null;

function startTimer() {
    time_required = time_required + 1;
    var min = Math.floor((time_required%3600)/60);
    var sec = Math.floor((time_required%3600)%60);
    var _time = min + ":" + sec;    
    $("#practice_timer").html(_time);
}

function reset_question_state(){
  questions = [],
  question_index=0,
  previous_index=-1,
  next_index=1;
}

function set_current_question(){
    question = questions[question_index];
}

function set_next_question(){
    question = questions[question_index+1];
    if(question){
        question_index += 1;
        previous_index += 1;
        next_index += 1;
    }
}

function set_previous_question(){
    question = questions[question_index-1];
    if(question){
        question_index -= 1;
        previous_index -= 1;
        next_index -= 1;
    }
}

function render_question(){
    time_required = 0;
    attempts = 0;
    window.clearInterval(practice_timer);
    practice_timer = setInterval("startTimer()", 1000);
    if(typeof question == 'undefined'){
		var first_question = questions[0];
        $('#quest').html('<h2>Question of this chapter are over please select another chapter.</h2>');
		window.clearInterval(practice_timer);
		add_mixpanel_event("Completed practice", { "subject_id": m_subject_name, "chapter_id": m_chapter_id});
	}
    else{
        var content = '';
        /* For Explanation Button Logic*/
		
        content += '<h3 class="question-text app-section">'+question.text+'</h3>';
        content += '<div style="width:100%;"><p  class=" app-section question-option '+ (question.answer_option == 1 ? '' : 'in')+'correct" id="option-1" ><span class="marker" style="display:none;width:5%;margin-right:0.1em"><img src="images/'+(question.answer_option == 1 ? '' : 'in')+'correct.png"></span>'+question.option_1+'</p>';
        content += '<p class="app-section question-option '+ (question.answer_option == 2 ? '' : 'in')+'correct" id="option-2" ><span class="marker" style="display:none;width:5%;margin-right:0.1em"><img src="images/'+(question.answer_option == 2 ? '' : 'in')+'correct.png"></span>'+question.option_2+'</p>';
        content += '<p class="question-option app-section '+ (question.answer_option == 3 ? '' : 'in')+'correct" id="option-3" ><span class="marker" style="display:none;width:5%;margin-right:0.1em"><img src="images/'+(question.answer_option == 3 ? '' : 'in')+'correct.png"></span>'+question.option_3+'</p>';
        content += '<p class="question-option app-section '+ (question.answer_option == 4 ? '' : 'in')+'correct" id="option-4" ><span class="marker" style="display:none;width:5%;margin-right:0.1em"><img src="images/'+(question.answer_option == 4 ? '' : 'in')+'correct.png"></span>'+question.option_4+'</p>';
        
        var expl=question.explanation;
        if(expl != null){
			content += '<a href="#openModal" role="button" style="margin-left: 20px;text-decoration:none !important;"><span class="app-button orange" style="padding: 10px;">View Explanation</span></a>';
		}
		content += '</div>';
		content += '<div style="padding-bottom: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;</div>';
		
        content += '<div style="position:fixed;bottom:0;width:100%">';
        if(previous_index < 0){
            content += '<span style="display:inline-block;width:30%;color:grey" class="app-button white" id="previous-question" data-back="true">Back</span>';
        }else{
            content += '<span style="display:inline-block;width:30%" class="app-button blue" id="previous-question">Previous</span>';
        }
        content += '<span style="display:inline-block;width:40%" class="app-button orange" id="skip-question">Skip</span>';//Testing Purpose
        if(next_index == questions.length){
            content += '<span style="display:inline-block;width:30%;color:grey;" class="app-button white" id="next-question" data-back="true">Done</span>';
        }else{
            content += '<span style="display:inline-block;width:30%" class="app-button green" id="next-question">Next</span>';
        }
		content += '</div>';
		if(expl != null){
			content += '<div id="openModal" class="modalDialog" >';
            content += '<div style="overflow:scroll;"><a href="#close" title="Close" class="close" style="float:right;"> Close</a><h2> Explanation :</h2>';
            content += '<p><h3 class="question-text" >'+expl+'</h3></p></div></div>';	
        }
        $('#quest').html(content);
        //MathJax.Hub.Typeset();
		MathJax.Hub.Queue(['Typeset',MathJax.Hub]);
		
        $('#previous-question').click(function(){
            if($(this).hasClass('white')){
                App.load('practice');
            }else{
                set_previous_question();
                render_question();
            }
        });
        var enableNext = false;
        $('#next-question').click(function(event){
            if (enableNext) {
			     
                //html5sql.process('update questions set attempted = 1, time_required='+ time_required +', attempts='+ attempts+' where id='+question.id);
				html5sql.process('update questions set attempted = 1, time_required='+ time_required +', attempts='+ attempts+' , updated_at='+'date('+'"now"'+')'+' where id='+question.id);
				//alert("updated date");
                if($(this).hasClass('white')){
                    App.load('practice');
                }else{
                    set_next_question();
                    render_question();
                }
            } else {
                App.dialog({
                  title        : 'Select correct option',
                  text         : 'Please select correct answer to move on next question.',
                  okButton     : 'Try Again',
                }, function (tryAgain) {
                });    
            }
        });
        $('#skip-question').click(function(){
            set_next_question();
            render_question();
        });
        $('.correct').click(function(event){
            attempts = attempts < 4 ? attempts + 1 : attempts;
            enableNext = true;
            $('.marker').hide();
            $(event.target).find('.marker').animate({display: 'inline'}, 500, 'ease-out');
        });
        $('.incorrect').click(function(event){
            attempts = attempts < 4 ? attempts + 1 : attempts;
            enableNext = false;
            $('.marker').hide();
            $(event.target).find('.marker').animate({display: 'inline'}, 500, 'ease-out');
        });
    }
}


App.controller('question', function(page){
    // App.removeFromStack(0,1);
    $(page).find("#back_practice_que").click(function(){
       App.load('practice');
		//App.load('chapters');
    });
});
/* Utilities */
function log_sql_error(error, statement){
  console.log("Error: "+error.message+" when processing " + statement);
}

App.controller('chapters', function(page, subject){
    $(page).on('appShow', function(){
    $(page).find(".app-title").html(subject.name);
    html5sql.process('select * from chapters where subject_id='+subject.subject_id, function(transaction, results){
		var content = '';
        for(var i=0;i<results.rows.length;i++){
            var section = results.rows.item(i);
            content+='<li data-section="'+section.id+'">'+section.name;
            content+= '<span style="float:right;display:inline-block;padding:0.2rem" class="app-button blue start-button">practice</span></li>';
        }
        $('#chapter-list').html(content);
        $('.start-button').click(function(event){
            event.stopPropagation();
                var chapter_id = $(event.target).parent().data('section');
				m_chapter_id = chapter_id;
                html5sql.process('select id, attempted, text, option_1, option_2, option_3, option_4,explanation, answer_option, chapter_id, subject_id from questions where chapter_id='+chapter_id + ' order by attempted',
                    function(transaction, results){
                        reset_question_state();
                        for(var i=0;i<results.rows.length;i++){
                            questions.push(results.rows.item(i));
                        }
                        App.load('question', function(){
                            set_current_question();
                            render_question();
                        });
                    }, log_sql_error);
			add_mixpanel_event("Started practice - " + subject.name + " [ " + chapter_id  +" ]", {});
		});
        /*
        $('#chapter-list li').click(function(event){
            event.stopPropagation();
            var chapter_id = $(event.target).data('section');
            App.load('sub-chapters', {chapter_id: chapter_id});
        });
        */
    }, log_sql_error);

    });
    $(page).find("#back_chapters").click(function(){
        App.load('practice');
    });
    // App.removeFromStack(0,1);
	add_mixpanel_event("Sub - " + subject.name + " visited", {});
});

App.controller('practice', function(page){
	add_mixpanel_event("Practice tab visited", {});
    $(page).on('appShow', function(){
        var $template = $(page).find('.practice_subject');
        $(page).find('.practice_subject').remove();
        var $subject_list = $(page).find('#subject-list');
        var sub_ids = [];
        html5sql.process('select * from subjects order by id',
          function(transaction, results){
              for(var i=0; i<results.rows.length;i++){
                var $subject = $template.clone(true);
                var subject = results.rows.item(i);
                sub_ids.push(subject.id);
                $subject.attr({
                    "data-section": subject.id,
                    id: "practice_subject_"+subject.id,
					'data-name':subject.name
                });
				
                $subject.find('.practice_sub_name').html(subject.name)
                                                   .attr({'data-section': subject.id, 'data-name':subject.name});
				$subject.find('.start-button').attr({'data-section': subject.id, 'data-name':subject.name});
				if(subject.id == 1){
					$subject_list.append("<h5 style="+"'text-align:center;'"+"> First Year Subject's</h5>");
				}	
			   if(subject.id == 8){
					$subject_list.append("<h5 style="+"'text-align:center;'"+"> Second Year Subject's</h5>");
				}
				$subject_list.append($subject);
              }
              //$subject_list.append("</ul> <h6 style="+"'text-align:center;'"+">Trial version of Web Portal and Android app will be available to every one from Nov 1, 2014 to Nov 30, 2014. From Dec 1,2014 only Deeper registered students can access full updated APP and Web portal.</h6>");
			 //Commented Following Notice for Gurkul Release.
			 //$subject_list.append("</ul> <h6 style="+"'text-align:center;'"+">Trial Version Period extended till DEC 15,2014. Please register to Deeper Exams to avail full benefits. Visit www.deeper.co.in</h6>");
			  
              $('.start-button_1').click(function(event){
                  event.stopPropagation();
                  var subject_id = $(event.target).parent().data('section');
                  html5sql.process('select id, attempted, text, option_1, option_2, option_3, option_4,explanation, answer_option, time_required, attempts from questions where subject_id='+subject_id + 'and attempted <> 1 order by attempted',
                      function(transaction, results){
                          reset_question_state();
                          for(var i=0;i<results.rows.length;i++){
                              questions.push(results.rows.item(i));
                          }
                          App.load('question',function(){
                              set_current_question();
                              render_question();
                          });
                      }, log_sql_error);
              }, log_sql_error);
              $('#subject-list li').click(function(event){
                  event.stopPropagation();
                  var subject_id = $(event.target).data('section');
				  var subject_name = $(event.target).attr('data-name');
                  m_subject_name = subject_name;
				  App.load('chapters', {subject_id: subject_id, name: subject_name});
				  
              });
            
          },
          log_sql_error
        );
    });
});