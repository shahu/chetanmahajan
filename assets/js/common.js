App.load('loader');
//var db_name = 'Deeper_database_05Nov2014.db';
var db_name = 'Gurukul_Database_10_JAN_2015.db';

try {
    MathJax.Hub.Config({
	 extensions: ["tex2jax.js", "toMathML.js"],
	  jax: ["input/TeX", "output/HTML-CSS"],
	  tex2jax: {
		inlineMath: [ ['$','$'], ["\\(","\\)"] ],
		displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
		processEscapes: true
	  },
	  "HTML-CSS":{
		  linebreaks:{automatic:true},
		  availableFonts:["TeX"],
		  scale:75
	  },
	  "SVG":{
			 linebreaks:{automatic:true},
			  scale:75
	  },
	  AsciiMath: {
		fixphi: true,
		useMathMLspacing: true,
		displaystyle: false,
		decimalsign: "."
	  },
	   TeX: {
		Macros: {
		  RR: '{\\bf R}',
		  bold: ['{\\bf #1}', 1]
		},
		extensions: ['AMSmath.js','AMSsymbols.js', 'noErrors.js','noUndefined.js']
	  }
	});
} catch(e) {}

(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
//mixpanel.init("d4391bb39f1d6c6520868186b5433b87");//Developement Key
mixpanel.init("159f93559306fb7d746f5ace99b4c324"); // Production key


function add_mixpanel_event(event, options) {
  var c = $.extend(options, {"user_id": urlParam("user_id"), "email": urlParam("email"), "db_name": db_name});
  mixpanel.track(event, options);
}
function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}


try {

	html5sql.openDatabase(db_name, 'Spy DB', 1024*1024*3); //Production
} catch(e) {
	console.log(e.message);
	App.load('error');
	add_mixpanel_event("Error - LoadingDB", { "msg": e.message});
}

html5sql.process('select count(*) from subjects',
    function(){
      console.log("DB already created");
      updatedDB();
	  //startActivity(); //Do Not delete used for testing analysis part after updation of DB
    },
    function(error){
        $.get('data.sql', function(sqlStatements){
        html5sql.process(
            sqlStatements,
            function(){
                console.log("New DB created.");
				updatedDB();
				//startActivity(); //Do Not delete used for testing analysis part after updation of DB
			},
            log_sql_error
        );
    });
});
function updatedDB() {
    console.log("updatedDB called !!!");
	//New data_[version_name].sql file i.e Data which you want add to existing database of app
	$.get('data_2.sql', function(sqlStatements){
		console.log("Load all Sql statements from new data.sql ");
		html5sql.changeVersion("","2",sqlStatements,function(){
					console.log("DB updated to new version .");
					startActivity();
				},log_sql_error);
	});
	startActivity();
}
