 function handleAndroidBack(){
    if(!App.back()){
        Jasandra.back();
    }
}
function startActivity()
{
	App.load('analysis');
}
function log_sql_error(error, statement){
  console.log("Error: "+error.message+" when processing " + statement);
}
App.controller('analysis', function(page){ 
    var subjects = [];
    var chapters = [];
	
	html5sql.process('select * from subjects',
      function(transaction, results){  
        for(var i=0; i<results.rows.length;i++){
            var subject = results.rows.item(i);
            subjects.push(subject);
        }
        findAccuracy(subjects);	
		//findAccur(subjects,chapters);
      },
      log_sql_error
    );
	
    
    function findAccuracy(subjects) {
        var $template = $(page).find('.analysis_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
			
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select subject_id,updated_at, sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id,
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var total_attempts = results.rows.item(0)['sum(attempts)'];
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
                if (total_questions > 0) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                };
              },
              log_sql_error
            );
			console.log($accuracy);
            $analysis.append($accuracy);
			
        }
		$('.analysis_template').click(function(event){
			event.stopPropagation();
			var subject_id = $(event.target).data('section');
			var subject_name = $(event.target).attr('data-name');
			App.load('chapters_analysis', {id: subject_id, name: subject_name});
			
		});
		$('.button-todaysAnalysis').click(function(event){
			event.stopPropagation();
			//var subject_id = $(event.target).data('section');
			//var subject_name = $(event.target).attr('data-name');
			var date = new Date();
		    var current_date = date.toISOString().substring(0, 10);
			//App.load('todays_analysis');
			App.load('todays_analysis', {todays_date: current_date });
		});		
    } 		 
	
 add_mixpanel_event("Analysis page visited", {});
});


App.controller('chapters_analysis', function(page, subject){
	var chapters = [];		
	$(page).on('appShow', function(){
		$(page).find(".app-title").html(subject.name);
		add_mixpanel_event(" Chapter Analysis ["+subject.name+"]", {});	
		html5sql.process('select * from chapters where subject_id='+subject.id, function(transaction, results){
			for(var k=0; k<results.rows.length;k++){
				var chapter = results.rows.item(k);
				chapters.push(chapter);
			}
			findChapterAccuracy(chapters);
		},log_sql_error);	
    });
   
	function findChapterAccuracy(chapters) {
        var $template = $(page).find('.chapter_analysis_template').remove();
        var $analysis = $(page).find('#chap_analysis_content');
        for(var i=0; i<chapters.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_chap_"+chapters[i].id,
							"data-section": chapters[i].id,
							"data-name": chapters[i].name 
			});
            $accuracy.find(".accuracy_chapter_name").html(chapters[i].name);
            html5sql.process('select chapter_id, subject_id, sum(attempts), sum(time_required), count(*) from questions where attempted=1 and chapter_id='+chapters[i].id,
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_chap_"+results.rows.item(0)['chapter_id']);
                var total_attempts = results.rows.item(0)['sum(attempts)'];
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
                if (total_questions > 0) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);
                    $sub_analysis_div.find('.accuracy_chap_attempted_ques').html("Att Que: " +total_questions);
                    $sub_analysis_div.find('.accuracy_chap_eff').html("Acc: " + efficiency+" %");
                    $sub_analysis_div.find('.accuracy_chap_avg_time').html("Avg t: "+avg_time + " s");
                };
              },log_sql_error);			
            $analysis.append($accuracy);
        }	
    }
	 $(page).find("#back_chapters").click(function(){
		App.load('analysis');
		
    });
});


App.controller('todays_analysis', function(page,date){
	var subjects = [];
	var count=0;
	var currentDate=date.todays_date;
	html5sql.process('select * from subjects',
      function(transaction, results){  
        for(var i=0; i<results.rows.length;i++){
            var subject = results.rows.item(i);
            subjects.push(subject);
        }
        findTodaysAccuracy(subjects,currentDate);	
      },
      log_sql_error
    );
	
   function findTodaysAccuracy(subjects,currentDate) {
         //alert("currentDate="+currentDate);
        var $template = $(page).find('.analysis_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
		
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select  subject_id,  strftime('+'"%Y-%m-%d"'+',updated_at'+')'+' AS dbDate , sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id , // +' and strftime('+'"%Y-%m-%d"'+',updated_at'+')'+'='+temp,
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var $dbDateTime = results.rows.item(0)['dbDate'];
                var total_attempts = results.rows.item(0)['sum(attempts)'];
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
				if (total_questions > 0 && $dbDateTime == currentDate) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                };
              },
              log_sql_error

            );
			$analysis.append($accuracy);
			/*
			//Previous  Action
			$(page).find('.button_previous_analysis').click(function(event){
				//alert("previous Button clicked !!! ="+subjects.length+"currentDate"+currentDate);
				findPrevAccuracy(subjects,currentDate);	
			});
			//Next Action
			$(page).find('.button_next_analysis').click(function(event){
				//alert("NEXT Button clicked !!! ="+subjects.length);	
				findNextAccuracy(subjects,currentDate);
			});
			*/	
        }		
    }
	/*
	$(page).find('.button_previous_analysis').click(function(event){
            alert("previous Button clicked !!!");			 
			findPrevAccuracy(subjects,currentDate);
	});
	$(page).find('.button_next_analysis').click(function(event){
            alert("NEXT Button clicked !!!");			 
			findNextAccuracy(subjects,currentDate);
	});
	*/
    $(page).find("#back_todaysAnalysis").click(function(){
		App.load('analysis');
    });
    
	function findPrevAccuracy(subjects,currentDate){
	
       alert("findPrevAccuracy::subjects==>"+subjects.length+",currentDate="+currentDate);	
		
	    var $template = $(page).find('.analysis_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
		  /*$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
		
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
			*/
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select  subject_id, date'+'("'+currentDate+'",'+'"-1 days"'+') AS previousDate,strftime('+'"%Y-%m-%d"'+',updated_at'+')'+' AS dbDate , sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id+'  and strftime('+'"%Y-%m-%d"'+',updated_at'+')'+'=date'+'("'+currentDate+'",'+'"-1 days"'+')',
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var $dbDateTime = results.rows.item(0)['dbDate'];
				var previousDate = results.rows.item(0)['previousDate'];
				
				alert(" previousDate=["+previousDate+"], updated_at =["+$dbDateTime+"]");
			
   			    //alert("$dbDateTime===================>"+$dbDateTime);
                var total_attempts = results.rows.item(0)['sum(attempts)'];
				//alert("total_attempts===================>"+total_attempts);
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
				//alert("total_questions ===================>"+total_questions);
				if (total_questions > 0 && $dbDateTime == previousDate) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                }
				else{
						alert(" You havent Practice on Previous Days");
						findTodaysAccuracy(subjects,currentDate);
				};
              },
              log_sql_error
            );
			$analysis.append($accuracy);			
        }
    }
	
	
	function findNextAccuracy(subjects,currentDate){
	
		//alert("findPrevAccuracy::subjects==>"+subjects.length+",currentDate="+currentDate);		
	    var $template = $(page).find('.next_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
			
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select  subject_id, date'+'( "'+currentDate+'",'+'"1 days"'+') AS nextDate,strftime('+'"%Y-%m-%d"'+',updated_at'+')'+' AS dbDate , sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id+'  and strftime('+'"%Y-%m-%d"'+',updated_at'+')'+'=date'+'( "'+currentDate+'",'+'"1 days"'+')',
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var $dbDateTime = results.rows.item(0)['dbDate'];
				var nextDate = results.rows.item(0)['nextDate'];
				//alert(" nextDate=["+nextDate+"], Today's Date=["+$dbDateTime+"]");
				//alert("$dbDateTime===================>"+$dbDateTime);
                var total_attempts = results.rows.item(0)['sum(attempts)'];
				//alert("total_attempts===================>"+total_attempts);
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
				if (total_questions > 0 && $dbDateTime == nextDate) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                };
              },
              log_sql_error
            );
			$analysis.append($accuracy);			
        }
    }
	
});
/*
App.controller('previous_analysis', function(page){    
	
	var subjects = [];
   	html5sql.process('select * from subjects',
      function(transaction, results){  
        for(var i=0; i<results.rows.length;i++){
            var subject = results.rows.item(i);
            subjects.push(subject);
        }
        findPrevAccuracy(subjects);	
      },
      log_sql_error
    );
	
	function findPrevAccuracy(subjects){	
	    var $template = $(page).find('.analysis_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
			
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select  subject_id, date'+'('+'"now"'+','+'"-1 days"'+') AS previousDate,strftime('+'"%Y-%m-%d"'+',updated_at'+')'+' AS dbDate , sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id+'  and strftime('+'"%Y-%m-%d"'+',updated_at'+')'+'=date'+'('+'"now"'+','+'"-1 days"'+')',
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var $dbDateTime = results.rows.item(0)['dbDate'];
				var previousDate = results.rows.item(0)['previousDate'];
				alert(" previousDate=["+previousDate+"], Today's Date=["+$dbDateTime+"]");
				//alert("$dbDateTime===================>"+$dbDateTime);
                var total_attempts = results.rows.item(0)['sum(attempts)'];
				//alert("total_attempts===================>"+total_attempts);
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
				if (total_questions > 0 && $dbDateTime == previousDate) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                };
              },
              log_sql_error
            );
			$analysis.append($accuracy);			
        }
    }
	$(page).find('.button_previous_analysis').click(function(event){
            alert("previous Button clicked !!!");			 
			App.load('previous_analysis');
	});
	$(page).find('.button_next_analysis').click(function(event){
            alert("NEXT Button clicked !!!");			 
			App.load('next_analysis');
	});
    $(page).find("#back_todaysAnalysis").click(function(){
		App.load('analysis');
    });
	
});
App.controller('next_analysis', function(page){    
	
	var subjects = [];
   	html5sql.process('select * from subjects',
      function(transaction, results){  
        for(var i=0; i<results.rows.length;i++){
            var subject = results.rows.item(i);
            subjects.push(subject);
        }
        findNextAccuracy(subjects);	
      },
      log_sql_error
    );
	
	function findNextAccuracy(subjects){	
	    var $template = $(page).find('.analysis_template').remove();
        var $analysis = $(page).find('#analysis_content');
        for(var i=0; i<subjects.length;i++){
            var $accuracy = $template.clone(true);
            $accuracy.attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			$accuracy.find("div").attr({"id": "analysis_"+subjects[i].id,
							"data-section": subjects[i].id,
							"data-name": subjects[i].name 
			});
			
			$accuracy.find(".accuracy_subject_name").attr({"id": "analysis_sub_"+subjects[i].id});
			$accuracy.find(".accuracy_attempted_ques").attr({"id": "attempted_ques_"+subjects[i].id});
			$accuracy.find(".accuracy_sub_eff").attr({"id": "sub_eff_"+subjects[i].id});
            $accuracy.find(".accuracy_sub_avg_time").attr({"id": "sub_avg_time_"+subjects[i].id});
			
            $accuracy.find(".accuracy_subject_name").html(subjects[i].name);
            html5sql.process('select  subject_id, date'+'('+'"now"'+','+'"1 days"'+') AS previousDate,strftime('+'"%Y-%m-%d"'+',updated_at'+')'+' AS dbDate , sum(attempts), sum(time_required), count(*) from questions where attempted=1 and subject_id='+subjects[i].id+'  and strftime('+'"%Y-%m-%d"'+',updated_at'+')'+'=date'+'('+'"now"'+','+'"1 days"'+')',
              function(transaction, results){
                var efficiency =0, avg_time=0, total_questions=0;
                var $sub_analysis_div = $(page).find("#analysis_"+results.rows.item(0)['subject_id']);
				var $dbDateTime = results.rows.item(0)['dbDate'];
				var previousDate = results.rows.item(0)['previousDate'];
				alert(" previousDate=["+previousDate+"], Today's Date=["+$dbDateTime+"]");
				//alert("$dbDateTime===================>"+$dbDateTime);
                var total_attempts = results.rows.item(0)['sum(attempts)'];
				//alert("total_attempts===================>"+total_attempts);
                var total_time_req = results.rows.item(0)['sum(time_required)'];
                total_questions = results.rows.item(0)['count(*)'];
				if (total_questions > 0 && $dbDateTime == previousDate) {
                    efficiency = parseFloat((total_questions/total_attempts)*100).toFixed(2);
                    avg_time = parseFloat(total_time_req/total_questions).toFixed(2);    
                    $sub_analysis_div.find('.accuracy_attempted_ques').html("Att Que:"+total_questions);
                    $sub_analysis_div.find('.accuracy_sub_eff').html("Acc:"+efficiency+"%");
                    $sub_analysis_div.find('.accuracy_sub_avg_time').html("Avg t:"+avg_time+" s");
                };
              },
              log_sql_error
            );
			$analysis.append($accuracy);			
        }
    }
	$(page).find('.button_previous_analysis').click(function(event){
            alert("previous Button clicked !!!");			 
			App.load('previous_analysis');
	});
	$(page).find('.button_next_analysis').click(function(event){
            alert("NEXT Button clicked !!!");			 
			App.load('next_analysis');
	});
    $(page).find("#back_todaysAnalysis").click(function(){
		App.load('analysis');
    });
	
});
*/